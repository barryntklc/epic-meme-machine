﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Epic_Meme_Machine
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {


            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            EpicGamer("I can do this all day");
        }

        static void Yeetus()
        {
            Random randomizer = new Random();
            int eeLength = randomizer.Next(2, 10);
            char[] y = { 'y', 'Y' };
            char[] e = { 'e', 'E' };
            char[] t = { 't', 'T' };
            Console.Write(y[randomizer.Next(0, 2)]);
            for (int i = 0; i < eeLength; i++)
            {
                Console.Write(e[randomizer.Next(0, 2)]);
            }
            Console.Write(t[randomizer.Next(0, 2)]);
            Console.WriteLine();
        }

        static void EpicGamer(String input)
        {
            //TODO modes: uppercase random or uppercase vowels and random

            Random randomizer = new Random();

            int sw = 0;
            foreach (char c in input)
            {
                sw = randomizer.Next(0, 2);
                if (sw == 0)
                {
                    Console.Write(c.ToString().ToLower());
                } else
                {
                    Console.Write(c.ToString().ToUpper());
                }
            }
            Console.WriteLine();
        }
    }
}
